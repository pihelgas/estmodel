/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model.type;

import ee.envir.estmodel.EstModel.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public abstract class Configurable<C extends Parameterizable> extends Adjustable {

    private List<C> configurations = new ArrayList<>();

    protected abstract C findConfiguration(Parameter parameter);

    @NotNull
    public List<@Valid @NotNull C> getConfigurations() {
        return this.configurations;
    }

    public void setConfigurations(List<C> configurations) {
        this.configurations = configurations;
    }

}
