/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model.deserializer;

import ee.envir.estmodel.EstModel.DiffuseSource;
import ee.envir.estmodel.model.config.ArableLandConfiguration;
import ee.envir.estmodel.model.config.ForestConfiguration;
import ee.envir.estmodel.model.config.GrasslandConfiguration;
import ee.envir.estmodel.model.config.PastureConfiguration;
import ee.envir.estmodel.model.config.PeatlandConfiguration;
import ee.envir.estmodel.model.config.UrbanConfiguration;
import ee.envir.estmodel.model.config.WaterConfiguration;
import ee.envir.estmodel.model.config.WetlandConfiguration;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.serializer.DeserializationContext;
import jakarta.json.bind.serializer.JsonbDeserializer;
import jakarta.json.stream.JsonParser;
import java.lang.reflect.Type;

public final class DiffuseSourceDeserializer implements JsonbDeserializer<DiffuseSource> {

    private static final Jsonb CONTEXT = JsonbBuilder.create();

    @Override
    public DiffuseSource deserialize(JsonParser parser, DeserializationContext context, Type type) {

        var jo = parser.getObject();
        var ds = CONTEXT.fromJson(jo.toString(), DiffuseSource.class);

        if (!ds.getConfigurations().isEmpty()) {

            var configurations = jo.get("configurations").asJsonArray();

            ds.setConfigurations(switch (ds.getType()) {
                case ARABLE_LAND ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), ArableLandConfiguration.class));
                case FOREST ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), ForestConfiguration.class));
                case GRASSLAND ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), GrasslandConfiguration.class));
                case PASTURE ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), PastureConfiguration.class));
                case PEATLAND ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), PeatlandConfiguration.class));
                case URBAN ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), UrbanConfiguration.class));
                case WATER ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), WaterConfiguration.class));
                case WETLAND ->
                    configurations.getValuesAs(c -> CONTEXT
                    .fromJson(c.toString(), WetlandConfiguration.class));
            });

        }

        return ds;

    }

}
