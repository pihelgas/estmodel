/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel.model;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import static java.lang.Double.NaN;
import static java.lang.Double.isNaN;
import java.time.LocalDate;
import java.util.Collection;
import static java.util.Comparator.reverseOrder;
import java.util.Optional;
import java.util.stream.Stream;
import static java.util.stream.Stream.concat;

public interface RiverBasin<R extends RiverBasin<R, S>, S extends RiverStation<S>>
        extends RiverPoint, Basin {

    default double area(double distance) {

        var lowerPoint = concat(this.getStations().stream(),
                this.getAdjustments().stream()).sorted(reverseOrder())
                .filter(s -> s.getDistance() <= distance)
                .findFirst();

        double lowerArea = lowerPoint
                .map(p -> p.getArea() - p.getOverlapArea()).orElse(this.getArea());

        double lowerDistance = lowerPoint
                .map(RiverPoint::getDistance).orElse(0.0);

        if (lowerDistance == distance || this.getLength() < lowerDistance) {
            return lowerArea;
        }

        var upperPoint = concat(this.getStations().stream(),
                this.getAdjustments().stream()).sorted()
                .filter(s -> s.getDistance() > distance)
                .findFirst();

        double upperArea = upperPoint.map(p -> p.getArea() - p.getOverlapArea())
                .orElseGet(() -> this.getTributaries().stream()
                .filter(r -> r.getDistance() >= this.getLength())
                .mapToDouble(r -> r.getArea() - r.getOverlapArea())
                .sum());

        if (this.getLength() <= distance && !upperPoint.isPresent()) {
            return upperArea;
        }

        double upperDistance = upperPoint.map(RiverPoint::getDistance)
                .orElse(this.getLength());

        var tributaries = this.getTributaries().stream()
                .filter(r -> r.getDistance() > lowerDistance
                && r.getDistance() < upperDistance)
                .toList();

        double areaDifference = lowerArea - upperArea - tributaries.stream()
                .mapToDouble(r -> r.getArea() - r.getOverlapArea())
                .sum();

        double distanceDifference = upperDistance - lowerDistance;

        double areaGrowth = areaDifference / distanceDifference;

        tributaries = tributaries.stream()
                .filter(r -> r.getDistance() >= distance)
                .toList();

        return upperArea + (upperDistance - distance) * areaGrowth
                + tributaries.stream()
                        .mapToDouble(r -> r.getArea() - r.getOverlapArea())
                        .sum();

    }

    default double calculationArea(double distance) {

        return this.calculationArea(distance, this.area(distance));

    }

    default double calculationArea(double distance, double area) {

        return area - concat(this.getAdjustments().stream(), this.getStations().stream())
                .filter(p -> p.getDistance() >= distance).sorted().findFirst()
                .map(p -> this.getTributaries().stream()
                .filter(r -> r.getDistance() >= distance && r.getDistance() < p.getDistance())
                .mapToDouble(RiverPoint::areaDifference)
                .sum() + p.areaDifference())
                .orElseGet(() -> this.getTributaries().stream()
                .filter(r -> r.getDistance() >= distance)
                .mapToDouble(RiverPoint::areaDifference)
                .sum());

    }

    @Override
    default double discharge(Object parameter, LocalDate startDate, LocalDate endDate) {

        double unmonitoredAreaSpecificDischarge = this.findReceivingStation(parameter, startDate, endDate)
                .map(s -> s.separateSpecificDischarge(parameter, startDate, endDate))
                .orElseGet(() -> this.getDistrict().specificDischarge(parameter, startDate, endDate));

        return this.discharge(unmonitoredAreaSpecificDischarge, parameter, startDate, endDate);

    }

    default double discharge(Object parameter, LocalDate startDate, LocalDate endDate, double distance) {

        return this.discharge(parameter, startDate, endDate, distance, this.calculationArea(distance));

    }

    default double discharge(Object parameter, LocalDate startDate, LocalDate endDate,
            double distance, double calculationArea) {

        var lowerStation = this.getStations().stream().sorted(reverseOrder())
                .filter(s -> s.getDistance() <= distance && s.isMonitoring(parameter, startDate, endDate))
                .findFirst();

        double lowerCalculationArea = lowerStation
                .map(RiverStation::getCalculationArea).orElse(this.getCalculationArea());

        double lowerDischarge = lowerStation
                .map(s -> s.discharge(parameter, startDate, endDate))
                .orElseGet(() -> this.discharge(parameter, startDate, endDate));

        double lowerDistance = lowerStation
                .map(RiverStation::getDistance).orElse(0.0);

        if (lowerDistance == distance || this.getLength() < lowerDistance) {
            return lowerDischarge / lowerCalculationArea * calculationArea;
        }

        double specificDischarge = lowerStation
                .map(s -> s.separateSpecificDischarge(parameter, startDate, endDate))
                .orElseGet(() -> this.findReceivingStation(parameter, startDate, endDate)
                .map(s -> s.separateSpecificDischarge(parameter, startDate, endDate))
                .orElseGet(() -> this.getDistrict().specificDischarge(parameter, startDate, endDate)));

        var upperStation = this.getStations().stream().sorted()
                .filter(s -> s.getDistance() > distance
                && s.isMonitoring(parameter, startDate, endDate))
                .findFirst();

        double upperCalculationArea = upperStation
                .map(RiverStation::getCalculationArea)
                .orElseGet(() -> this.getTributaries().stream()
                .filter(r -> r.getDistance() >= this.getLength())
                .mapToDouble(RiverBasin::getCalculationArea)
                .sum());

        double upperDischarge = upperStation
                .map(s -> s.discharge(parameter, startDate, endDate))
                .orElseGet(() -> this.getTributaries().stream()
                .filter(r -> r.getDistance() >= this.getLength())
                .mapToDouble(r -> r.discharge(specificDischarge, parameter, startDate, endDate))
                .sum());

        if (this.getLength() <= distance && !upperStation.isPresent()) {
            return upperDischarge + (calculationArea - upperCalculationArea)
                    * specificDischarge;
        }

        double upperDistance = upperStation
                .map(RiverStation::getDistance).orElse(this.getLength());

        var tributaries = this.getTributaries().stream()
                .filter(r -> r.getDistance() > lowerDistance
                && r.getDistance() < upperDistance)
                .toList();

        var adjustments = this.getAdjustments().stream()
                .filter(a -> a.getDistance() > lowerDistance
                && a.getDistance() < upperDistance)
                .toList();

        double areaDifference = lowerCalculationArea + adjustments.stream()
                .mapToDouble(a -> a.getCalculationArea() * (1 - a.getFactor())).sum()
                - upperCalculationArea
                - tributaries.stream().mapToDouble(RiverBasin::getCalculationArea).sum();

        double dischargeDifference = lowerDischarge + adjustments.stream()
                .mapToDouble(a -> a.getCalculationArea() * (1 - a.getFactor()) * specificDischarge)
                .sum() - upperDischarge - tributaries.stream().mapToDouble(r -> r.discharge(
                specificDischarge, parameter, startDate, endDate)).sum();

        double dischargeGrowth = dischargeDifference / areaDifference;

        tributaries = tributaries.stream()
                .filter(r -> r.getDistance() >= distance)
                .toList();

        adjustments = adjustments.stream()
                .filter(r -> r.getDistance() >= distance)
                .toList();

        return (calculationArea + adjustments.stream()
                .mapToDouble(a -> a.getCalculationArea() * (1 - a.getFactor()))
                .sum()
                - upperCalculationArea
                - tributaries.stream()
                        .mapToDouble(RiverBasin::getCalculationArea)
                        .sum())
                * dischargeGrowth
                + upperDischarge + tributaries.stream()
                        .mapToDouble(r -> r.discharge(specificDischarge, parameter, startDate, endDate))
                        .sum()
                - adjustments.stream()
                        .mapToDouble(a -> a.getCalculationArea() * (1 - a.getFactor()) * dischargeGrowth)
                        .sum();

    }

    default double discharge(double unmonitoredAreaSpecificDischarge,
            Object parameter, LocalDate startDate, LocalDate endDate) {

        if (isNaN(unmonitoredAreaSpecificDischarge)) {
            return NaN;
        }

        var station = this.getStations().stream().sorted()
                .filter(s -> s.isMonitoring(parameter, startDate, endDate))
                .findFirst();

        var tributaries = station.map(s -> this.getTributaries().stream()
                .filter(r -> r.getDistance() < s.getDistance()))
                .orElseGet(() -> this.getTributaries().stream()).toList();
        var adjustments = station.map(s -> this.getAdjustments().stream()
                .filter(a -> a.getDistance() < s.getDistance()))
                .orElseGet(() -> this.getAdjustments().stream()).toList();

        double monitoredArea = station.map(s -> s.getArea() - s.getOverlapArea()).orElse(0.0);
        double monitoredDischarge = station.map(s -> s.discharge(parameter, startDate, endDate)).orElse(0.0);

        return adjustments.stream().filter(a -> a.getDistance() == this.getLength()).findAny().map(adjustment -> {

            double unmonitoredArea = this.getArea() - monitoredArea - adjustment.getArea()
                    - tributaries.stream().mapToDouble(r -> r.getArea() - r.getOverlapArea()).sum();
            double unmonitoredDischarge = unmonitoredArea * unmonitoredAreaSpecificDischarge;

            return monitoredDischarge
                    + unmonitoredDischarge
                    + tributaries.stream()
                            .mapToDouble(r -> r.discharge(unmonitoredAreaSpecificDischarge, parameter, startDate, endDate))
                            .sum()
                    - this.getAdjustments().stream()
                            .filter(a -> a.getDistance() < this.getLength())
                            .mapToDouble(a -> a.getCalculationArea() * unmonitoredAreaSpecificDischarge * (1 - a.getFactor()))
                            .sum()
                    + adjustment.getCalculationArea() * unmonitoredAreaSpecificDischarge * adjustment.getFactor();
        }).orElseGet(() -> {

            double unmonitoredArea = this.getArea() - monitoredArea
                    - tributaries.stream().mapToDouble(r -> r.getArea() - r.getOverlapArea()).sum();

            double unmonitoredDischarge = unmonitoredArea * unmonitoredAreaSpecificDischarge;

            return monitoredDischarge
                    + unmonitoredDischarge
                    + tributaries.stream()
                            .mapToDouble(r -> r.discharge(unmonitoredAreaSpecificDischarge, parameter, startDate, endDate))
                            .sum()
                    - adjustments.stream()
                            .mapToDouble(a -> a.getCalculationArea() * unmonitoredAreaSpecificDischarge * (1 - a.getFactor()))
                            .sum();
        });

    }

    default Stream<S> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getStations().stream().sorted()
                .filter(s -> s.isMonitoring(parameter, startDate, endDate))
                .findFirst()
                .map(s -> concat(Stream.of(s), this.getTributaries().stream()
                .filter(r -> r.getDistance() < s.getDistance())
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate))))
                .orElseGet(() -> this.getTributaries().stream()
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate)));

    }

    default Stream<S> findEnclosingStations(Object parameter, LocalDate startDate, LocalDate endDate, double distance) {

        return this.getStations().stream().sorted()
                .filter(s -> s.getDistance() > distance && s.isMonitoring(parameter, startDate, endDate))
                .findFirst()
                .map(s -> concat(Stream.of(s), this.getTributaries().stream()
                .filter(r -> r.getDistance() > distance && r.getDistance() < s.getDistance())
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate))))
                .orElseGet(() -> this.getTributaries().stream()
                .filter(r -> r.getDistance() > distance)
                .flatMap(r -> r.findEnclosingStations(parameter, startDate, endDate)));

    }

    default Optional<S> findReceivingStation(Object parameter, LocalDate startDate, LocalDate endDate) {

        return this.getParentRiver()
                .filter(r -> r.getDistrict().equals(this.getDistrict()))
                .flatMap(r -> r.findReceivingStation(parameter, startDate, endDate, this.getDistance()));

    }

    default Optional<S> findReceivingStation(Object parameter, LocalDate startDate, LocalDate endDate, double distance) {

        return this.getStations().stream().sorted(reverseOrder())
                .filter(s -> s.getDistance() < distance && s.isMonitoring(parameter, startDate, endDate))
                .findFirst().or(() -> this.findReceivingStation(parameter, startDate, endDate));

    }

    @Positive
    double getLength();

    RiverBasinDistrict<S> getDistrict();

    Optional<R> getParentRiver();

    Collection<Adjustment> getAdjustments();

    Collection<S> getStations();

    Collection<R> getTributaries();

    @JsonbTransient
    default boolean isDistrictRiver() {

        return this.getTributaries().stream()
                .allMatch(r -> r.getDistrict().equals(this.getDistrict()) && r.isDistrictRiver());

    }

    default double specificDischarge(Object parameter, LocalDate startDate, LocalDate endDate, double distance) {

        return this.specificDischarge(parameter, startDate, endDate, distance, this.calculationArea(distance));

    }

    default double specificDischarge(Object parameter, LocalDate startDate, LocalDate endDate,
            double distance, double calculationArea) {

        if (calculationArea == 0) {
            return this.discharge(parameter, startDate, endDate, distance, calculationArea);
        } else {
            return this.discharge(parameter, startDate, endDate, distance, calculationArea)
                    / calculationArea;
        }

    }

    public interface Adjustment extends RiverPoint {

        @PositiveOrZero
        double getFactor();

        @Override
        default double areaDifference() {

            return this.getArea() - this.getOverlapArea() - this.getCalculationArea()
                    + this.getCalculationArea() * (1 - this.getFactor());

        }

    }

}
