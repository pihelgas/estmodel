/*
 * Copyright (C) 2020-2025 Estonian Environment Agency
 * Copyright (C) 2017-2019 Estonian Environmental Research Centre
 *
 * This file is part of EstModel.
 *
 * EstModel is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EstModel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with EstModel.  If not, see <https://www.gnu.org/licenses/>.
 */
package ee.envir.estmodel;

import ee.envir.estmodel.model.config.ArableLandConfiguration;
import ee.envir.estmodel.model.config.ForestConfiguration;
import ee.envir.estmodel.model.config.GrasslandConfiguration;
import ee.envir.estmodel.model.config.PastureConfiguration;
import ee.envir.estmodel.model.config.PeatlandConfiguration;
import ee.envir.estmodel.model.config.UrbanConfiguration;
import ee.envir.estmodel.model.config.WaterConfiguration;
import ee.envir.estmodel.model.config.WetlandConfiguration;
import ee.envir.estmodel.model.type.Identifiable;
import ee.envir.estmodel.model.type.Parameterizable;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbPropertyOrder;
import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import static java.lang.Double.isNaN;
import static java.lang.Double.max;
import static java.lang.Double.min;
import static java.lang.Math.abs;
import static java.time.Year.isLeap;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;
import static java.util.stream.Stream.concat;

@JsonbPropertyOrder({"year", "subcatchments"})
public class EstModel extends Identifiable implements Runnable {

    private int year;
    private List<Subcatchment> subcatchments = new ArrayList<>();

    @Override
    public void run() {
        this.findSubcatchments().forEach(subcatchment -> {
            this.estimate(subcatchment);
            this.adjust(subcatchment);
        });
    }

    public Stream<Subcatchment> findSubcatchments() {
        return this.getSubcatchments().stream()
                .flatMap(Subcatchment::findSubcatchments);
    }

    protected void estimate(Subcatchment subcatchment) {

        int days = isLeap(this.getYear()) ? 366 : 365;

        long seconds = days * 86400L;

        subcatchment.getPointSources()
                .forEach(ps -> ps.setEstimates(Stream.of(Parameter.TN, Parameter.TP)
                .map(parameter -> {

                    var config = ps.findConfiguration(parameter);

                    double riverRetentionTime = (ps.getDistance()
                            - subcatchment.getDistance()
                            - (ps.getLakeCourseLength()
                            - subcatchment.getLakeCourseLength())) * 1000
                            / subcatchment.getFlowVelocity() / 86400; // day

                    double riverRetentionFactor = riverRetentionTime
                            * config.getRiverMaxRetentionFactor()
                            / (config.getRiverHalfRetentionTime() + riverRetentionTime);

                    double retentionFactor = riverRetentionFactor
                            * (ps.getDistance() - subcatchment.getDistance()
                            - (ps.getLakeCourseLength() - subcatchment.getLakeCourseLength()))
                            / (ps.getDistance() - subcatchment.getDistance())
                            + config.getLakeAverageRetentionFactor()
                            * (ps.getLakeCourseLength() - subcatchment.getLakeCourseLength())
                            / (ps.getDistance() - subcatchment.getDistance());

                    if (ps.getDistance() - subcatchment.getDistance() <= 0) {
                        retentionFactor = 0;
                    }

                    double load = ps.getMeasurements().stream()
                            .filter(parameter)
                            .mapToDouble(Measurement::getValue)
                            .sum();

                    var estimate = new PointSource.Estimate(parameter);
                    if (ps.getWaterType() == PointSource.WaterType.STORMWATER
                            || ps.getWaterType() == PointSource.WaterType.TREATED_WATER
                            || ps.getWaterType() == PointSource.WaterType.WASTEWATER) {

                        estimate.setRetention(load * retentionFactor);
                        estimate.setLoad(load * (1 - retentionFactor));

                    }
                    return estimate;

                }).toList()));

        double scFlow = subcatchment.getWaterDischarge()
                - subcatchment.getSubcatchments().stream()
                        .mapToDouble(Subcatchment::getWaterDischarge)
                        .sum(); // m3/s

        double scArea = subcatchment.getDiffuseSources().stream()
                .mapToDouble(DiffuseSource::getArea)
                .sum(); // km2

        double waterVolume = subcatchment.getDiffuseSources().stream()
                .filter(DiffuseSource.Type.WATER)
                .mapToDouble(DiffuseSource::getArea)
                .sum() * 1000000 * subcatchment.getAverageLakeDepth()
                + subcatchment.getDiffuseSources().stream()
                        .filter(DiffuseSource.Type.WATER.negate())
                        .mapToDouble(DiffuseSource::getArea)
                        .sum() //km2
                * subcatchment.getStreamDensity() // m/km2
                * subcatchment.getAverageRiverWidth() // m
                * subcatchment.getAverageRiverDepth(); // m

        subcatchment.getDiffuseSources().forEach(ds -> {

            double flow = scFlow * seconds
                    / scArea * ds.getArea(); // m3/a

            ds.setEstimates(Stream.of(Parameter.TN, Parameter.TP).map(parameter -> {

                var config = ds.findConfiguration(parameter);

                double retentionTime = waterVolume / (scFlow * 60 * 60 * 24); // day

                double retentionFactor = retentionTime
                        * config.getRiverMaxRetentionFactor()
                        / (config.getRiverHalfRetentionTime() + retentionTime);

                if (isNaN(retentionFactor)) {
                    retentionFactor = 0;
                }

                double drainageFactor = ds.getDrainedArea() / ds.getArea()
                        * config.getDrainageFactor();

                double fertilizerFactor = 0;

                if (ds.getHighLandUseIntensityArea() > 0) {
                    fertilizerFactor = ds.getFertilizers().stream()
                            .filter(parameter)
                            .mapToDouble(Fertilizer::getAmount)
                            .sum()
                            / ds.getHighLandUseIntensityArea()
                            / config.getMaxFertilizerAmount()
                            * config.getFertilizerFactor();
                }

                double fertileSoilFactor = ds.getFertileSoilArea() / ds.getArea()
                        * config.getFertileSoilFactor();
                double peatSoilFactor = ds.getPeatSoilArea() / ds.getArea()
                        * config.getPeatSoilFactor();
                double sandSoilFactor = ds.getSandSoilArea() / ds.getArea()
                        * config.getSandSoilFactor();

                double lsFactor = ds.getLsFactor();

                if (ds.getLsFactor() < 1) {
                    lsFactor *= 0.1;
                } else {
                    lsFactor = 1;
                }

                lsFactor *= config.getLsFactor();

                double landUseIntensityFactor = config.getHighLandUseIntensityFactor()
                        * ds.getHighLandUseIntensityArea() / ds.getArea()
                        + config.getMediumLandUseIntensityFactor()
                        * ds.getMediumLandUseIntensityArea() / ds.getArea();

                double anthropogenicFactor = (landUseIntensityFactor
                        + drainageFactor + fertilizerFactor
                        + config.getCoverFactor() + lsFactor + fertileSoilFactor
                        + abs(sandSoilFactor) + abs(peatSoilFactor));

                if (anthropogenicFactor > 1) {
                    anthropogenicFactor = 1 + (anthropogenicFactor - 1) / 2;
                }

                double naturalRaisingFactor = DoubleStream.of(fertileSoilFactor, lsFactor)
                        .sum();

                double naturalLoweringFactor = DoubleStream.of(sandSoilFactor)
                        .sum();

                naturalRaisingFactor = min(naturalRaisingFactor, 1);
                naturalRaisingFactor = max(naturalRaisingFactor, 0);

                naturalLoweringFactor = min(naturalLoweringFactor, 0);
                naturalLoweringFactor = max(naturalLoweringFactor, -1);

                double naturalConcentration = config.getInitialNaturalConcentration()
                        + naturalRaisingFactor * (config.getMaxNaturalConcentration() - config.getInitialNaturalConcentration())
                        - naturalLoweringFactor * (config.getInitialNaturalConcentration() - config.getMinNaturalConcentration());

                naturalConcentration = min(naturalConcentration, config.getMaxNaturalConcentration());
                naturalConcentration = max(naturalConcentration, config.getMinNaturalConcentration());

                var estimate = new DiffuseSource.Estimate(parameter);

                estimate.setNaturalLoad(flow * naturalConcentration / 1000000);

                estimate.setUnconnectedDwellingsLoad(ds.getUnconnectedPopulation()
                        * config.getPopulationEquivalent() / 1000000 * days);
                estimate.setManagedLandLoad(flow * ((config.getMaxConcentration()
                        - config.getMinConcentration()) * anthropogenicFactor) / 1000000);
                estimate.setAtmosphericDeposition(ds.getArea()
                        * config.getAtmosphericSpecificDeposition() / 1000);

                estimate.setAtmosphericRetention(estimate.getAtmosphericDeposition()
                        * retentionFactor);
                estimate.setManagedLandRetention(estimate.getManagedLandLoad()
                        * retentionFactor);
                estimate.setNaturalRetention(estimate.getNaturalLoad()
                        * retentionFactor);
                estimate.setUnconnectedDwellingsRetention(estimate.getUnconnectedDwellingsLoad()
                        * config.getUnconnectedDwellingsRetentionFactor());
                estimate.setStormwaterRetention(0);

                estimate.setManagedLandLoad(estimate.getManagedLandLoad()
                        * (1 - retentionFactor));
                estimate.setUnconnectedDwellingsLoad(estimate.getUnconnectedDwellingsLoad()
                        * (1 - config.getUnconnectedDwellingsRetentionFactor()));
                estimate.setAtmosphericDeposition(estimate.getAtmosphericDeposition()
                        * (1 - retentionFactor));
                estimate.setNaturalLoad(estimate.getNaturalLoad()
                        * (1 - retentionFactor));
                estimate.setStormwaterLoad(estimate.getStormwaterLoad()
                        * (1 - retentionFactor));

                return estimate;

            }).toList());

        });

        subcatchment.getSubcatchments().forEach(sc -> sc.findSubcatchments().forEach(ssc -> {

            ssc.getPointSources().forEach(ps -> ps.getEstimates().forEach(estimate -> {

                var config = subcatchment.findConfiguration(estimate.getParameter());

                double riverRetentionTime = (sc.getDistance() - subcatchment.getDistance()
                        - (sc.getLakeCourseLength() - subcatchment.getLakeCourseLength())) * 1000
                        / subcatchment.getFlowVelocity() / 86400; // day

                double riverRetentionFactor = riverRetentionTime
                        * config.getRiverMaxRetentionFactor()
                        / (config.getRiverHalfRetentionTime() + riverRetentionTime);

                double retentionFactor = riverRetentionFactor
                        * (sc.getDistance() - subcatchment.getDistance()
                        - (sc.getLakeCourseLength() - subcatchment.getLakeCourseLength()))
                        / (sc.getDistance() - subcatchment.getDistance())
                        + config.getLakeAverageRetentionFactor()
                        * (sc.getLakeCourseLength() - subcatchment.getLakeCourseLength())
                        / (sc.getDistance() - subcatchment.getDistance());

                if (sc.getDistance() - subcatchment.getDistance() <= 0) {
                    retentionFactor = 0;
                }

                estimate.setRetention(estimate.getRetention() + estimate.getLoad() * retentionFactor);
                estimate.setLoad(estimate.getLoad() * (1 - retentionFactor));

            }));

            ssc.getDiffuseSources().forEach(ds -> ds.getEstimates().forEach(estimate -> {

                var config = subcatchment.findConfiguration(estimate.getParameter());

                double riverRetentionTime = (sc.getDistance() - subcatchment.getDistance()
                        - (sc.getLakeCourseLength() - subcatchment.getLakeCourseLength())) * 1000
                        / subcatchment.getFlowVelocity() / 86400; // day

                double riverRetentionFactor = riverRetentionTime
                        * config.getRiverMaxRetentionFactor()
                        / (config.getRiverHalfRetentionTime() + riverRetentionTime);

                double retentionFactor = riverRetentionFactor
                        * (sc.getDistance() - subcatchment.getDistance()
                        - (sc.getLakeCourseLength() - subcatchment.getLakeCourseLength()))
                        / (sc.getDistance() - subcatchment.getDistance())
                        + config.getLakeAverageRetentionFactor()
                        * (sc.getLakeCourseLength() - subcatchment.getLakeCourseLength())
                        / (sc.getDistance() - subcatchment.getDistance());

                if (sc.getDistance() - subcatchment.getDistance() <= 0) {
                    retentionFactor = 0;
                }

                estimate.setAtmosphericRetention(estimate.getAtmosphericRetention()
                        + estimate.getAtmosphericDeposition() * retentionFactor);
                estimate.setManagedLandRetention(estimate.getManagedLandRetention()
                        + estimate.getManagedLandLoad() * retentionFactor);
                estimate.setNaturalRetention(estimate.getNaturalRetention()
                        + estimate.getNaturalLoad() * retentionFactor);
                estimate.setUnconnectedDwellingsRetention(estimate.getUnconnectedDwellingsRetention()
                        + estimate.getUnconnectedDwellingsLoad() * retentionFactor);
                estimate.setStormwaterRetention(estimate.getStormwaterRetention()
                        + estimate.getStormwaterLoad() * retentionFactor);

                estimate.setManagedLandLoad(estimate.getManagedLandLoad()
                        * (1 - retentionFactor));
                estimate.setAtmosphericDeposition(estimate.getAtmosphericDeposition()
                        * (1 - retentionFactor));
                estimate.setNaturalLoad(estimate.getNaturalLoad()
                        * (1 - retentionFactor));
                estimate.setUnconnectedDwellingsLoad(estimate.getUnconnectedDwellingsLoad()
                        * (1 - retentionFactor));
                estimate.setStormwaterLoad(estimate.getStormwaterLoad()
                        * (1 - retentionFactor));

            }));

        }));

    }

    protected void adjust(Subcatchment subcatchment) {

        int days = isLeap(this.getYear()) ? 366 : 365;

        long seconds = days * 86400L;

        double scArea = subcatchment.getDiffuseSources().stream()
                .mapToDouble(DiffuseSource::getArea)
                .sum(); // km2

        double scFlow = subcatchment.getWaterDischarge()
                - subcatchment.getSubcatchments().stream()
                        .mapToDouble(Subcatchment::getWaterDischarge)
                        .sum(); // m3/s

        subcatchment.getDiffuseSources()
                .forEach(ds -> ds.getAdjustments().forEach(ds::apply));

        subcatchment.getPointSources()
                .forEach(ps -> ps.getAdjustments().forEach(ps::apply));

        Stream.of(Parameter.TN, Parameter.TP)
                .forEach(parameter -> subcatchment.getMeasurements().stream()
                .filter(parameter)
                .reduce(Measurement::sum).ifPresent(m -> m.adjust(subcatchment)));

        subcatchment.getAdjustments().forEach(subcatchment::apply);

        subcatchment.getDiffuseSources()
                .forEach(ds -> ds.getEstimates().forEach(estimate -> {

            var config = ds.findConfiguration(estimate.getParameter());

            double totalLoad = estimate.getNaturalLoad()
                    + estimate.getManagedLandLoad()
                    + estimate.getUnconnectedDwellingsLoad()
                    + estimate.getStormwaterLoad();

            double flow = scFlow * seconds
                    / scArea * ds.getArea(); // m3/a

            double naturalRetentionFactor = estimate.getNaturalRetention()
                    / (estimate.getNaturalLoad() + estimate.getNaturalRetention());

            double naturalConcentration = (estimate.getNaturalLoad()
                    / (1 - naturalRetentionFactor))
                    / flow * 1000000;

            double naturalAdjustmentFactor;

            if (naturalConcentration < config.getMinNaturalConcentration() && naturalConcentration > 0) {

                naturalAdjustmentFactor = config.getMinNaturalConcentration()
                        / naturalConcentration;

            } else if (naturalConcentration > config.getMaxNaturalConcentration()) {

                naturalAdjustmentFactor = config.getMaxNaturalConcentration()
                        / naturalConcentration;

            } else {
                return;
            }

            double scatteredDwellingsLoadRatio = 0;
            double stormwaterLoadRatio = 0;
            double managedLandLoadRatio = 0;

            if (totalLoad != estimate.getNaturalLoad()) {
                scatteredDwellingsLoadRatio = estimate.getUnconnectedDwellingsLoad()
                        / (totalLoad - estimate.getNaturalLoad());
                if (scatteredDwellingsLoadRatio != 1) {
                    stormwaterLoadRatio = estimate.getStormwaterLoad()
                            / (totalLoad - estimate.getNaturalLoad() - estimate.getUnconnectedDwellingsLoad());
                    managedLandLoadRatio = estimate.getManagedLandLoad()
                            / (totalLoad - estimate.getNaturalLoad() - estimate.getUnconnectedDwellingsLoad());
                }
            }

            double scatteredDwellingsInitialLoad = estimate.getUnconnectedDwellingsLoad()
                    + estimate.getUnconnectedDwellingsRetention();
            double stormWaterInitialLoad = estimate.getStormwaterLoad()
                    + estimate.getStormwaterRetention();
            double managedLandInitialLoad = estimate.getManagedLandLoad()
                    + estimate.getManagedLandRetention();

            estimate.setNaturalLoad(estimate.getNaturalLoad() * naturalAdjustmentFactor);

            if (estimate.getNaturalLoad() != 0) {
                estimate.setNaturalRetention(estimate.getNaturalLoad()
                        * naturalRetentionFactor / (1 - naturalRetentionFactor));
            }

            if (estimate.getNaturalLoad() < totalLoad) {

                estimate.setUnconnectedDwellingsLoad((totalLoad - estimate.getNaturalLoad())
                        * scatteredDwellingsLoadRatio);

                if (estimate.getUnconnectedDwellingsLoad() > scatteredDwellingsInitialLoad) {
                    estimate.setUnconnectedDwellingsLoad(scatteredDwellingsInitialLoad);
                }
                estimate.setUnconnectedDwellingsRetention(scatteredDwellingsInitialLoad
                        - estimate.getUnconnectedDwellingsLoad());

                estimate.setStormwaterLoad((totalLoad - estimate.getUnconnectedDwellingsLoad()
                        - estimate.getNaturalLoad()) * stormwaterLoadRatio);

                if (estimate.getStormwaterLoad() != 0) {
                    double stormWaterRetentionFactor = estimate.getStormwaterRetention()
                            / stormWaterInitialLoad;
                    estimate.setStormwaterRetention(estimate.getStormwaterLoad()
                            * stormWaterRetentionFactor / (1 - stormWaterRetentionFactor));
                }

                estimate.setManagedLandLoad((totalLoad
                        - estimate.getUnconnectedDwellingsLoad()
                        - estimate.getNaturalLoad())
                        * managedLandLoadRatio);

                if (estimate.getManagedLandLoad() != 0) {
                    double managedLandRetentionFactor = estimate.getManagedLandRetention()
                            / managedLandInitialLoad;
                    estimate.setManagedLandRetention(estimate.getManagedLandLoad()
                            * managedLandRetentionFactor
                            / (1 - managedLandRetentionFactor));
                }

                estimate.setUnknownLoad(totalLoad
                        - estimate.getNaturalLoad()
                        - estimate.getUnconnectedDwellingsLoad()
                        - estimate.getStormwaterLoad()
                        - estimate.getManagedLandLoad());
                estimate.setUnknownRetention(estimate.getUnknownLoad()
                        * naturalRetentionFactor / (1 - naturalRetentionFactor));

            } else {
                estimate.setManagedLandLoad(0);
                estimate.setManagedLandRetention(managedLandInitialLoad);
                estimate.setUnconnectedDwellingsLoad(0);
                estimate.setUnconnectedDwellingsRetention(scatteredDwellingsInitialLoad);
                estimate.setStormwaterLoad(0);
                estimate.setStormwaterRetention(stormWaterInitialLoad);
                estimate.setNaturalLoad(totalLoad);
                estimate.setNaturalRetention(estimate.getNaturalLoad()
                        * naturalRetentionFactor / (1 - naturalRetentionFactor));
            }

        }));

    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @NotNull
    public List<@Valid @NotNull Subcatchment> getSubcatchments() {
        return this.subcatchments;
    }

    public void setSubcatchments(List<Subcatchment> subcatchments) {
        this.subcatchments = subcatchments;
    }

    @JsonbPropertyOrder({"distance", "lakeCourseLength", "averageLakeDepth",
        "averageRiverDepth", "averageRiverWidth", "streamDensity", "flowVelocity",
        "waterDischarge", "measurements", "adjustments", "configurations",
        "diffuseSources", "pointSources", "subcatchments"})
    public static class Subcatchment extends Identifiable {

        private double distance;
        private double lakeCourseLength;
        private double averageLakeDepth = 3.5;
        private double averageRiverDepth = 1.3;
        private double averageRiverWidth = 3.5;
        private double streamDensity = 420;
        private double flowVelocity = 0.2;
        private double waterDischarge;
        private List<Measurement> measurements = new ArrayList<>();
        private List<Adjustment> adjustments = new ArrayList<>();
        private List<Configuration> configurations = new ArrayList<>();
        private List<DiffuseSource> diffuseSources = new ArrayList<>();
        private List<PointSource> pointSources = new ArrayList<>();
        private List<Subcatchment> subcatchments = new ArrayList<>();

        protected void apply(Adjustment adjustment) {
            this.getDiffuseSources().forEach(ds -> ds.apply(adjustment));
        }

        protected Configuration findConfiguration(Parameter parameter) {
            return this.getConfigurations().stream()
                    .filter(parameter)
                    .findFirst().orElseGet(() -> new Configuration(parameter));
        }

        public Stream<Subcatchment> findSubcatchments() {
            return concat(this.getSubcatchments().stream()
                    .flatMap(Subcatchment::findSubcatchments), Stream.of(this));
        }

        @PositiveOrZero
        public double getDistance() {
            return this.distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        @PositiveOrZero
        public double getLakeCourseLength() {
            return this.lakeCourseLength;
        }

        public void setLakeCourseLength(double length) {
            this.lakeCourseLength = length;
        }

        @PositiveOrZero
        public double getAverageLakeDepth() {
            return this.averageLakeDepth;
        }

        public void setAverageLakeDepth(double depth) {
            this.averageLakeDepth = depth;
        }

        @PositiveOrZero
        public double getAverageRiverDepth() {
            return this.averageRiverDepth;
        }

        public void setAverageRiverDepth(double depth) {
            this.averageRiverDepth = depth;
        }

        @PositiveOrZero
        public double getAverageRiverWidth() {
            return this.averageRiverWidth;
        }

        public void setAverageRiverWidth(double width) {
            this.averageRiverWidth = width;
        }

        @PositiveOrZero
        public double getStreamDensity() {
            return this.streamDensity;
        }

        public void setStreamDensity(double density) {
            this.streamDensity = density;
        }

        @Positive
        public double getFlowVelocity() {
            return this.flowVelocity;
        }

        public void setFlowVelocity(double velocity) {
            this.flowVelocity = velocity;
        }

        @Positive
        public double getWaterDischarge() {
            return this.waterDischarge;
        }

        public void setWaterDischarge(double discharge) {
            this.waterDischarge = discharge;
        }

        @NotNull
        public List<@Valid @NotNull Measurement> getMeasurements() {
            return this.measurements;
        }

        public void setMeasurements(List<Measurement> measurements) {
            this.measurements = measurements;
        }

        @NotNull
        public List<@Valid @NotNull Adjustment> getAdjustments() {
            return this.adjustments;
        }

        public void setAdjustments(List<Adjustment> adjustments) {
            this.adjustments = adjustments;
        }

        @NotNull
        public List<@Valid @NotNull Configuration> getConfigurations() {
            return this.configurations;
        }

        public void setConfigurations(List<Configuration> configurations) {
            this.configurations = configurations;
        }

        @NotNull
        public List<@Valid @NotNull DiffuseSource> getDiffuseSources() {
            return this.diffuseSources;
        }

        public void setDiffuseSources(List<DiffuseSource> sources) {
            this.diffuseSources = sources;
        }

        @NotNull
        public List<@Valid @NotNull PointSource> getPointSources() {
            return this.pointSources;
        }

        public void setPointSources(List<PointSource> sources) {
            this.pointSources = sources;
        }

        @NotNull
        public List<@Valid @NotNull Subcatchment> getSubcatchments() {
            return this.subcatchments;
        }

        public void setSubcatchments(List<Subcatchment> subcatchments) {
            this.subcatchments = subcatchments;
        }

        public static class Configuration extends EstModel.Configuration {

            protected double lakeAverageRetentionFactor;

            @JsonbCreator
            public Configuration(@NotNull Parameter parameter) {
                super(parameter);
                this.lakeAverageRetentionFactor = switch (parameter) {
                    case TN ->
                        0.33;
                    case TP ->
                        0.28;
                };
            }

            public double getLakeAverageRetentionFactor() {
                return this.lakeAverageRetentionFactor;
            }

            public void setLakeAverageRetentionFactor(double factor) {
                this.lakeAverageRetentionFactor = factor;
            }

        }

    }

    @JsonbPropertyOrder({"type", "area", "mediumLandUseIntensityArea",
        "highLandUseIntensityArea", "drainedArea", "claySoilArea",
        "fertileSoilArea", "peatSoilArea", "sandSoilArea",
        "unconnectedPopulation", "lsFactor", "fertilizers",
        "adjustments", "configurations", "estimates"})
    public static class DiffuseSource extends Identifiable {

        private Type type;
        private double area;
        private double mediumLandUseIntensityArea;
        private double highLandUseIntensityArea;
        private double drainedArea;
        private double claySoilArea;
        private double fertileSoilArea;
        private double peatSoilArea;
        private double sandSoilArea;
        private double unconnectedPopulation;
        private double lsFactor;
        private List<Fertilizer> fertilizers = new ArrayList<>();
        private List<Adjustment> adjustments = new ArrayList<>();
        private List<Configuration> configurations = new ArrayList<>();
        private List<Estimate> estimates = new ArrayList<>();

        protected void apply(Adjustment adjustment) {
            this.getEstimates().forEach(e -> e.apply(adjustment));
        }

        protected Configuration findConfiguration(Parameter parameter) {
            return this.getConfigurations().stream()
                    .filter(parameter)
                    .findFirst().orElseGet(() -> switch (this.getType()) {
                case ARABLE_LAND ->
                    new ArableLandConfiguration(parameter);
                case FOREST ->
                    new ForestConfiguration(parameter);
                case GRASSLAND ->
                    new GrasslandConfiguration(parameter);
                case PASTURE ->
                    new PastureConfiguration(parameter);
                case PEATLAND ->
                    new PeatlandConfiguration(parameter);
                case URBAN ->
                    new UrbanConfiguration(parameter);
                case WATER ->
                    new WaterConfiguration(parameter);
                case WETLAND ->
                    new WetlandConfiguration(parameter);
            });
        }

        @NotNull
        public Type getType() {
            return this.type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        @Positive
        public double getArea() {
            return this.area;
        }

        public void setArea(double area) {
            this.area = area;
        }

        @PositiveOrZero
        public double getMediumLandUseIntensityArea() {
            return this.mediumLandUseIntensityArea;
        }

        public void setMediumLandUseIntensityArea(double area) {
            this.mediumLandUseIntensityArea = area;
        }

        @PositiveOrZero
        public double getHighLandUseIntensityArea() {
            return this.highLandUseIntensityArea;
        }

        public void setHighLandUseIntensityArea(double area) {
            this.highLandUseIntensityArea = area;
        }

        @PositiveOrZero
        public double getDrainedArea() {
            return this.drainedArea;
        }

        public void setDrainedArea(double area) {
            this.drainedArea = area;
        }

        @PositiveOrZero
        public double getClaySoilArea() {
            return this.claySoilArea;
        }

        public void setClaySoilArea(double area) {
            this.claySoilArea = area;
        }

        @PositiveOrZero
        public double getFertileSoilArea() {
            return this.fertileSoilArea;
        }

        public void setFertileSoilArea(double area) {
            this.fertileSoilArea = area;
        }

        @PositiveOrZero
        public double getPeatSoilArea() {
            return this.peatSoilArea;
        }

        public void setPeatSoilArea(double area) {
            this.peatSoilArea = area;
        }

        @PositiveOrZero
        public double getSandSoilArea() {
            return this.sandSoilArea;
        }

        public void setSandSoilArea(double area) {
            this.sandSoilArea = area;
        }

        @PositiveOrZero
        public double getUnconnectedPopulation() {
            return this.unconnectedPopulation;
        }

        public void setUnconnectedPopulation(double count) {
            this.unconnectedPopulation = count;
        }

        @PositiveOrZero
        public double getLsFactor() {
            return this.lsFactor;
        }

        public void setLsFactor(double factor) {
            this.lsFactor = factor;
        }

        @NotNull
        public List<@Valid @NotNull Fertilizer> getFertilizers() {
            return this.fertilizers;
        }

        public void setFertilizers(List<Fertilizer> fertilizers) {
            this.fertilizers = fertilizers;
        }

        @NotNull
        public List<@Valid @NotNull Adjustment> getAdjustments() {
            return this.adjustments;
        }

        public void setAdjustments(List<Adjustment> adjustments) {
            this.adjustments = adjustments;
        }

        @NotNull
        public List<@Valid @NotNull Configuration> getConfigurations() {
            return this.configurations;
        }

        public void setConfigurations(List<Configuration> configurations) {
            this.configurations = configurations;
        }

        public List<Estimate> getEstimates() {
            return this.estimates;
        }

        @JsonbTransient
        public void setEstimates(List<Estimate> estimates) {
            this.estimates = estimates;
        }

        @JsonbPropertyOrder({"riparianZoneRetentionFactor",
            "unconnectedDwellingsRetentionFactor", "populationEquivalent",
            "mediumLandUseIntensityFactor", "highLandUseIntensityFactor",
            "coverFactor", "drainageFactor", "claySoilFactor", "fertileSoilFactor",
            "peatSoilFactor", "sandSoilFactor", "lsFactor", "fertilizerFactor",
            "maxFertilizerAmount", "atmosphericSpecificDeposition",
            "minConcentration", "maxConcentration", "minNaturalConcentration",
            "initialNaturalConcentration", "maxNaturalConcentration"})
        public static class Configuration extends EstModel.Configuration {

            private double riparianZoneRetentionFactor;
            private double unconnectedDwellingsRetentionFactor = 0.95;
            private double populationEquivalent;
            private double mediumLandUseIntensityFactor;
            private double highLandUseIntensityFactor;
            private double coverFactor;
            private double drainageFactor;
            private double claySoilFactor;
            private double fertileSoilFactor;
            private double peatSoilFactor;
            private double sandSoilFactor;
            private double lsFactor = 0.1;
            private double fertilizerFactor;
            private double maxFertilizerAmount;
            private double atmosphericSpecificDeposition;
            private double minConcentration;
            private double maxConcentration;
            private double minNaturalConcentration;
            private double initialNaturalConcentration;
            private double maxNaturalConcentration;

            @JsonbCreator
            public Configuration(@NotNull Parameter parameter) {
                super(parameter);
                switch (parameter) {
                    case TN -> {
                        this.atmosphericSpecificDeposition = 440.0 * 420 / 1000000;
                        this.riverHalfRetentionTime = 2;
                        this.minNaturalConcentration = 0.4;
                        this.initialNaturalConcentration = 1.21;
                        this.maxNaturalConcentration = 3;
                        this.maxFertilizerAmount = 25000;
                        this.populationEquivalent = 12;
                    }
                    case TP -> {
                        this.atmosphericSpecificDeposition = 8.1 * 420 / 1000000;
                        this.riverHalfRetentionTime = 3;
                        this.minNaturalConcentration = 0.02;
                        this.initialNaturalConcentration = 0.04;
                        this.maxNaturalConcentration = 0.06;
                        this.maxFertilizerAmount = 10000;
                        this.populationEquivalent = 1.5;
                    }
                    default ->
                        throw new IllegalArgumentException();

                }
                this.minConcentration = this.initialNaturalConcentration;
                this.maxConcentration = this.initialNaturalConcentration;
            }

            @PositiveOrZero
            public double getRiparianZoneRetentionFactor() {
                return this.riparianZoneRetentionFactor;
            }

            public void setRiparianZoneRetentionFactor(double factor) {
                this.riparianZoneRetentionFactor = factor;
            }

            @PositiveOrZero
            public double getUnconnectedDwellingsRetentionFactor() {
                return this.unconnectedDwellingsRetentionFactor;
            }

            public void setUnconnectedDwellingsRetentionFactor(double factor) {
                this.unconnectedDwellingsRetentionFactor = factor;
            }

            @PositiveOrZero
            public double getPopulationEquivalent() {
                return this.populationEquivalent;
            }

            public void setPopulationEquivalent(double pe) {
                this.populationEquivalent = pe;
            }

            public double getMediumLandUseIntensityFactor() {
                return this.mediumLandUseIntensityFactor;
            }

            public void setMediumLandUseIntensityFactor(double factor) {
                this.mediumLandUseIntensityFactor = factor;
            }

            public double getHighLandUseIntensityFactor() {
                return this.highLandUseIntensityFactor;
            }

            public void setHighLandUseIntensityFactor(double factor) {
                this.highLandUseIntensityFactor = factor;
            }

            @PositiveOrZero
            public double getCoverFactor() {
                return this.coverFactor;
            }

            public void setCoverFactor(double factor) {
                this.coverFactor = factor;
            }

            @PositiveOrZero
            public double getDrainageFactor() {
                return this.drainageFactor;
            }

            public void setDrainageFactor(double factor) {
                this.drainageFactor = factor;
            }

            public double getClaySoilFactor() {
                return this.claySoilFactor;
            }

            public void setClaySoilFactor(double factor) {
                this.claySoilFactor = factor;
            }

            public double getFertileSoilFactor() {
                return this.fertileSoilFactor;
            }

            public void setFertileSoilFactor(double factor) {
                this.fertileSoilFactor = factor;
            }

            public double getPeatSoilFactor() {
                return this.peatSoilFactor;
            }

            public void setPeatSoilFactor(double factor) {
                this.peatSoilFactor = factor;
            }

            public double getSandSoilFactor() {
                return this.sandSoilFactor;
            }

            public void setSandSoilFactor(double factor) {
                this.sandSoilFactor = factor;
            }

            @PositiveOrZero
            public double getLsFactor() {
                return this.lsFactor;
            }

            public void setLsFactor(double factor) {
                this.lsFactor = factor;
            }

            @PositiveOrZero
            public double getFertilizerFactor() {
                return this.fertilizerFactor;
            }

            public void setFertilizerFactor(double factor) {
                this.fertilizerFactor = factor;
            }

            @Positive
            public double getMaxFertilizerAmount() {
                return this.maxFertilizerAmount;
            }

            public void setMaxFertilizerAmount(double amount) {
                this.maxFertilizerAmount = amount;
            }

            @PositiveOrZero
            public double getAtmosphericSpecificDeposition() {
                return this.atmosphericSpecificDeposition;
            }

            public void setAtmosphericSpecificDeposition(double deposition) {
                this.atmosphericSpecificDeposition = deposition;
            }

            @PositiveOrZero
            public double getMinConcentration() {
                return this.minConcentration;
            }

            public void setMinConcentration(double concentration) {
                this.minConcentration = concentration;
            }

            @PositiveOrZero
            public double getMaxConcentration() {
                return this.maxConcentration;
            }

            public void setMaxConcentration(double concentration) {
                this.maxConcentration = concentration;
            }

            @PositiveOrZero
            public double getMinNaturalConcentration() {
                return this.minNaturalConcentration;
            }

            public void setMinNaturalConcentration(double concentration) {
                this.minNaturalConcentration = concentration;
            }

            @PositiveOrZero
            public double getInitialNaturalConcentration() {
                return this.initialNaturalConcentration;
            }

            public void setInitialNaturalConcentration(double concentration) {
                this.initialNaturalConcentration = concentration;
            }

            @PositiveOrZero
            public double getMaxNaturalConcentration() {
                return this.maxNaturalConcentration;
            }

            public void setMaxNaturalConcentration(double concentration) {
                this.maxNaturalConcentration = concentration;
            }

        }

        @JsonbPropertyOrder({"naturalLoad", "naturalRetention",
            "atmosphericDeposition", "atmosphericRetention",
            "stormwaterLoad", "stormwaterRetention",
            "managedLandLoad", "managedLandRetention",
            "unconnectedDwellingsLoad", "unconnectedDwellingsRetention",
            "unknownLoad", "unknownRetention",})
        public static class Estimate extends Parameterizable {

            private double naturalLoad;
            private double naturalRetention;
            private double atmosphericDeposition;
            private double atmosphericRetention;
            private double stormwaterLoad;
            private double stormwaterRetention;
            private double managedLandLoad;
            private double managedLandRetention;
            private double unconnectedDwellingsLoad;
            private double unconnectedDwellingsRetention;
            private double unknownLoad;
            private double unknownRetention;

            private Estimate() {
                super(null);
            }

            @JsonbCreator
            public Estimate(@NotNull Parameter parameter) {
                super(parameter);
            }

            private void apply(Adjustment adjustment) {

                if (this.getParameter() == adjustment.getParameter()) {

                    double unconnectedDwellingsInitialLoad = this.getUnconnectedDwellingsLoad()
                            + this.getUnconnectedDwellingsRetention();

                    this.setUnconnectedDwellingsLoad(this.getUnconnectedDwellingsLoad()
                            * adjustment.getFactor());

                    double adjustmentFactor = adjustment.getFactor();

                    if (this.getUnconnectedDwellingsLoad() > unconnectedDwellingsInitialLoad) {

                        adjustmentFactor = ((this.getNaturalLoad()
                                + this.getManagedLandLoad()
                                + this.getStormwaterLoad())
                                * adjustmentFactor
                                + (this.getUnconnectedDwellingsLoad()
                                - unconnectedDwellingsInitialLoad))
                                / (this.getNaturalLoad()
                                + this.getManagedLandLoad()
                                + this.getStormwaterLoad());

                        this.setUnconnectedDwellingsLoad(unconnectedDwellingsInitialLoad);
                    }
                    this.setUnconnectedDwellingsRetention(unconnectedDwellingsInitialLoad
                            - this.getUnconnectedDwellingsRetention());

                    if (this.getManagedLandLoad() != 0) {
                        double managedLandRetentionFactor = this.getManagedLandRetention()
                                / (this.getManagedLandLoad() + this.getManagedLandRetention());
                        this.setManagedLandLoad(this.getManagedLandLoad() * adjustmentFactor);
                        this.setManagedLandRetention(this.getManagedLandLoad()
                                * managedLandRetentionFactor / (1 - managedLandRetentionFactor));
                    }

                    if (this.getNaturalLoad() != 0) {
                        double naturalRetentionFactor = this.getNaturalRetention()
                                / (this.getNaturalLoad() + this.getNaturalRetention());
                        this.setNaturalLoad(this.getNaturalLoad()
                                * adjustmentFactor);
                        this.setNaturalRetention(this.getNaturalLoad()
                                * naturalRetentionFactor / (1 - naturalRetentionFactor));
                    }

                    if (this.getStormwaterLoad() != 0) {
                        double stormWaterRetentionFactor = this.getStormwaterRetention()
                                / (this.getStormwaterLoad() + this.getStormwaterRetention());
                        this.setStormwaterLoad(this.getStormwaterLoad() * adjustmentFactor);
                        this.setStormwaterRetention(this.getStormwaterLoad()
                                * stormWaterRetentionFactor / (1 - stormWaterRetentionFactor));
                    }

                }

            }

            public double getNaturalLoad() {
                return this.naturalLoad;
            }

            public void setNaturalLoad(double load) {
                this.naturalLoad = load;
            }

            public double getNaturalRetention() {
                return this.naturalRetention;
            }

            public void setNaturalRetention(double retention) {
                this.naturalRetention = retention;
            }

            public double getAtmosphericDeposition() {
                return this.atmosphericDeposition;
            }

            public void setAtmosphericDeposition(double deposition) {
                this.atmosphericDeposition = deposition;
            }

            public double getAtmosphericRetention() {
                return this.atmosphericRetention;
            }

            public void setAtmosphericRetention(double retention) {
                this.atmosphericRetention = retention;
            }

            public double getStormwaterLoad() {
                return this.stormwaterLoad;
            }

            public void setStormwaterLoad(double stormwaterLoad) {
                this.stormwaterLoad = stormwaterLoad;
            }

            public double getStormwaterRetention() {
                return this.stormwaterRetention;
            }

            public void setStormwaterRetention(double retention) {
                this.stormwaterRetention = retention;
            }

            public double getManagedLandLoad() {
                return this.managedLandLoad;
            }

            public void setManagedLandLoad(double load) {
                this.managedLandLoad = load;
            }

            public double getManagedLandRetention() {
                return this.managedLandRetention;
            }

            public void setManagedLandRetention(double retention) {
                this.managedLandRetention = retention;
            }

            public double getUnconnectedDwellingsLoad() {
                return this.unconnectedDwellingsLoad;
            }

            public void setUnconnectedDwellingsLoad(double load) {
                this.unconnectedDwellingsLoad = load;
            }

            public double getUnconnectedDwellingsRetention() {
                return this.unconnectedDwellingsRetention;
            }

            public void setUnconnectedDwellingsRetention(double retentsion) {
                this.unconnectedDwellingsRetention = retentsion;
            }

            public double getUnknownLoad() {
                return this.unknownLoad;
            }

            public void setUnknownLoad(double load) {
                this.unknownLoad = load;
            }

            public double getUnknownRetention() {
                return this.unknownRetention;
            }

            public void setUnknownRetention(double retention) {
                this.unknownRetention = retention;
            }

            public static Estimate sum(Estimate e1, Estimate e2) {

                if (e1.getParameter() != e2.getParameter()) {
                    throw new IllegalArgumentException();
                }

                var estimate = new Estimate(e1.getParameter());
                estimate.setManagedLandLoad(e1.getManagedLandLoad()
                        + e2.getManagedLandLoad());
                estimate.setManagedLandRetention(e1.getManagedLandRetention()
                        + e2.getManagedLandRetention());
                estimate.setUnconnectedDwellingsLoad(e1.getUnconnectedDwellingsLoad()
                        + e2.getUnconnectedDwellingsLoad());
                estimate.setUnconnectedDwellingsRetention(e1.getUnconnectedDwellingsRetention()
                        + e2.getUnconnectedDwellingsRetention());
                estimate.setStormwaterLoad(e1.getStormwaterLoad()
                        + e2.getStormwaterLoad());
                estimate.setStormwaterRetention(e1.getStormwaterRetention()
                        + e2.getStormwaterRetention());
                estimate.setAtmosphericDeposition(e1.getAtmosphericDeposition()
                        + e2.getAtmosphericDeposition());
                estimate.setAtmosphericRetention(e1.getAtmosphericRetention()
                        + e2.getAtmosphericRetention());
                estimate.setNaturalLoad(e1.getNaturalLoad()
                        + e2.getNaturalLoad());
                estimate.setNaturalRetention(e1.getNaturalRetention()
                        + e2.getNaturalRetention());
                estimate.setUnknownLoad(e1.getUnknownLoad()
                        + e2.getUnknownLoad());
                estimate.setUnknownRetention(e1.getUnknownRetention()
                        + e2.getUnknownRetention());
                return estimate;

            }

            public double totalLoad() {
                return this.getNaturalLoad()
                        + this.getAtmosphericDeposition()
                        + this.getStormwaterLoad()
                        + this.getManagedLandLoad()
                        + this.getUnconnectedDwellingsLoad()
                        + this.getUnknownLoad();
            }

            public double totalRetention() {
                return this.getNaturalRetention()
                        + this.getAtmosphericRetention()
                        + this.getStormwaterRetention()
                        + this.getManagedLandRetention()
                        + this.getUnconnectedDwellingsRetention()
                        + this.getUnknownRetention();
            }

        }

        public enum Type implements Predicate<DiffuseSource> {

            ARABLE_LAND,
            FOREST,
            GRASSLAND,
            PASTURE,
            PEATLAND,
            URBAN,
            WATER,
            WETLAND;

            @Override
            public boolean test(DiffuseSource source) {
                return this == source.type;
            }

        }

    }

    @JsonbPropertyOrder({"type", "waterType", "waterIntakeType",
        "wastewaterTreatmentPlant", "distance", "lakeCourseLength", "waterDischarge",
        "measurements", "adjustments", "configurations", "estimates"})
    public static class PointSource extends Identifiable {

        private Type type;
        private WaterType waterType;
        private WaterIntakeType waterIntakeType;
        private WastewaterTreatmentPlant wastewaterTreatmentPlant;
        private double distance;
        private double lakeCourseLength;
        private double waterDischarge;
        private List<Measurement> measurements = new ArrayList<>();
        private List<Adjustment> adjustments = new ArrayList<>();
        private List<Configuration> configurations = new ArrayList<>();
        private List<Estimate> estimates = new ArrayList<>();

        protected void apply(Adjustment adjustment) {
            this.getEstimates().forEach(e -> e.apply(adjustment));
        }

        protected Configuration findConfiguration(Parameter parameter) {
            return this.getConfigurations().stream()
                    .filter(parameter)
                    .findFirst().orElseGet(() -> new Configuration(parameter));
        }

        @NotNull
        public Type getType() {
            return this.type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        @NotNull
        public WaterType getWaterType() {
            return this.waterType;
        }

        public void setWaterType(WaterType type) {
            this.waterType = type;
        }

        @NotNull
        public WaterIntakeType getWaterIntakeType() {
            return this.waterIntakeType;
        }

        public void setWaterIntakeType(WaterIntakeType type) {
            this.waterIntakeType = type;
        }

        public Optional<WastewaterTreatmentPlant> getWastewaterTreatmentPlant() {
            return Optional.ofNullable(this.wastewaterTreatmentPlant);
        }

        public void setWastewaterTreatmentPlant(WastewaterTreatmentPlant wwtp) {
            this.wastewaterTreatmentPlant = wwtp;
        }

        @PositiveOrZero
        public double getDistance() {
            return this.distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        @PositiveOrZero
        public double getLakeCourseLength() {
            return this.lakeCourseLength;
        }

        public void setLakeCourseLength(double length) {
            this.lakeCourseLength = length;
        }

        @PositiveOrZero
        public double getWaterDischarge() {
            return this.waterDischarge;
        }

        public void setWaterDischarge(double discharge) {
            this.waterDischarge = discharge;
        }

        @NotNull
        public List<@Valid @NotNull Measurement> getMeasurements() {
            return this.measurements;
        }

        public void setMeasurements(List<Measurement> measurements) {
            this.measurements = measurements;
        }

        @NotNull
        public List<@Valid @NotNull Adjustment> getAdjustments() {
            return this.adjustments;
        }

        public void setAdjustments(List<Adjustment> adjustments) {
            this.adjustments = adjustments;
        }

        @NotNull
        public List<@Valid @NotNull Configuration> getConfigurations() {
            return this.configurations;
        }

        public void setConfigurations(List<Configuration> configurations) {
            this.configurations = configurations;
        }

        public List<Estimate> getEstimates() {
            return this.estimates;
        }

        @JsonbTransient
        public void setEstimates(List<Estimate> estimates) {
            this.estimates = estimates;
        }

        public static class Configuration extends EstModel.Configuration {

            protected double lakeAverageRetentionFactor;

            @JsonbCreator
            public Configuration(@NotNull Parameter parameter) {
                super(parameter);
                this.lakeAverageRetentionFactor = switch (parameter) {
                    case TN ->
                        0.33;
                    case TP ->
                        0.28;
                };
            }

            public double getLakeAverageRetentionFactor() {
                return this.lakeAverageRetentionFactor;
            }

            public void setLakeAverageRetentionFactor(double factor) {
                this.lakeAverageRetentionFactor = factor;
            }

        }

        @JsonbPropertyOrder({"load", "retention"})
        public static class Estimate extends Parameterizable {

            private double load;
            private double retention;

            private Estimate() {
                super(null);
            }

            @JsonbCreator
            public Estimate(@NotNull Parameter parameter) {
                super(parameter);
            }

            private void apply(Adjustment adjustment) {

                if (this.getParameter() == adjustment.getParameter()) {

                    if (adjustment.getFactor() > 0) {
                        double initialLoad = this.getLoad() + this.getRetention();
                        this.setLoad(this.getLoad() * adjustment.getFactor());
                        this.setRetention(initialLoad - this.getLoad());
                    } else {
                        this.setLoad(0);
                        this.setRetention(0);
                    }

                }

            }

            public double getLoad() {
                return this.load;
            }

            public void setLoad(double load) {
                this.load = load;
            }

            public double getRetention() {
                return this.retention;
            }

            public void setRetention(double retention) {
                this.retention = retention;
            }

            public static Estimate sum(Estimate e1, Estimate e2) {

                if (e1.getParameter() != e2.getParameter()) {
                    throw new IllegalArgumentException();
                }

                var estimate = new Estimate(e1.getParameter());
                estimate.setLoad(e1.getLoad() + e2.getLoad());
                estimate.setRetention(e1.getRetention() + e2.getRetention());
                return estimate;

            }

        }

        public enum Type implements Predicate<PointSource> {

            AQUACULTURE, INDUSTRIAL, MUNICIPAL;

            @Override
            public boolean test(PointSource ps) {
                return this == ps.getType();
            }

        }

        public enum WaterIntakeType implements Predicate<PointSource> {

            GROUNDWATER,
            SEAWATER,
            SURFACE_WATER;

            @Override
            public boolean test(PointSource ps) {
                return this == ps.getWaterIntakeType();
            }

        }

        public enum WaterType implements Predicate<PointSource> {

            COOLANT_WATER,
            MINING_WATER,
            QUARRY_WATER,
            STORMWATER,
            TREATED_WATER,
            WASTEWATER;

            @Override
            public boolean test(PointSource ps) {
                return this == ps.getWaterType();
            }

        }

    }

    @JsonbPropertyOrder({"treatmentLevel", "populationEquivalent"})
    public static class WastewaterTreatmentPlant extends Identifiable {

        private int treatmentLevel;
        private double populationEquivalent;

        @Min(0)
        @Max(4)
        public int getTreatmentLevel() {
            return this.treatmentLevel;
        }

        public void setTreatmentLevel(int level) {
            this.treatmentLevel = level;
        }

        @PositiveOrZero
        public double getPopulationEquivalent() {
            return this.populationEquivalent;
        }

        public void setPopulationEquivalent(double pe) {
            this.populationEquivalent = pe;
        }

    }

    public enum Parameter implements Predicate<Parameterizable> {

        TN, TP;

        @Override
        public boolean test(Parameterizable object) {
            return this == object.getParameter();
        }

    }

    @JsonbPropertyOrder({"type", "amount"})
    public static class Fertilizer extends Parameterizable {

        private Type type;
        private double amount;

        @JsonbCreator
        public Fertilizer(@NotNull Parameter parameter) {
            super(parameter);
        }

        public Type getType() {
            return this.type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        @PositiveOrZero
        public double getAmount() {
            return this.amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public enum Type implements Predicate<Fertilizer> {

            MINERAL, ORGANIC;

            @Override
            public boolean test(Fertilizer fertilizer) {
                return this == fertilizer.type;
            }

        }

    }

    @JsonbPropertyOrder({"description", "value"})
    public static class Measurement extends Parameterizable {

        private String description;
        private double value;

        @JsonbCreator
        public Measurement(Parameter parameter) {
            super(parameter);
        }

        private void adjust(Subcatchment subcatchment) {

            var dsEstimate = subcatchment.getDiffuseSources().stream()
                    .flatMap(ds -> ds.getEstimates().stream())
                    .filter(this.getParameter())
                    .reduce(DiffuseSource.Estimate::sum)
                    .orElseGet(DiffuseSource.Estimate::new);

            var psEstimate = subcatchment.getPointSources().stream()
                    .flatMap(ps -> ps.getEstimates().stream())
                    .filter(this.getParameter())
                    .reduce(PointSource.Estimate::sum)
                    .orElseGet(PointSource.Estimate::new);

            var upperDsEstimate = subcatchment.getSubcatchments().stream()
                    .flatMap(Subcatchment::findSubcatchments)
                    .flatMap(sc -> sc.getDiffuseSources().stream())
                    .flatMap(ds -> ds.getEstimates().stream())
                    .filter(this.getParameter())
                    .reduce(DiffuseSource.Estimate::sum)
                    .orElseGet(DiffuseSource.Estimate::new);

            var upperPsEstimate = subcatchment.getSubcatchments().stream()
                    .flatMap(Subcatchment::findSubcatchments)
                    .flatMap(sc -> sc.getPointSources().stream())
                    .flatMap(ps -> ps.getEstimates().stream())
                    .filter(this.getParameter())
                    .reduce(PointSource.Estimate::sum)
                    .orElseGet(PointSource.Estimate::new);

            var adjustment = new Adjustment(this.getParameter());
            adjustment.setFactor((this.getValue()
                    - dsEstimate.getAtmosphericDeposition()
                    - psEstimate.getLoad()
                    - upperDsEstimate.totalLoad()
                    - upperPsEstimate.getLoad())
                    / (dsEstimate.getNaturalLoad()
                    + dsEstimate.getManagedLandLoad()
                    + dsEstimate.getUnconnectedDwellingsLoad()
                    + dsEstimate.getStormwaterLoad()));

            subcatchment.apply(adjustment);

        }

        @Size(max = 255)
        public String getDescription() {
            return this.description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @PositiveOrZero
        public double getValue() {
            return this.value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public static Measurement sum(Measurement m1, Measurement m2) {

            if (m1.getParameter() != m2.getParameter()) {
                throw new IllegalArgumentException();
            }

            Measurement measurement = new Measurement(m1.getParameter());
            measurement.setValue(m1.getValue() + m2.getValue());
            return measurement;

        }

    }

    public static class Adjustment extends Parameterizable {

        private double factor = 1;

        @JsonbCreator
        public Adjustment(@NotNull Parameter parameter) {
            super(parameter);
        }

        @PositiveOrZero
        public double getFactor() {
            return this.factor;
        }

        public void setFactor(double factor) {
            this.factor = factor;
        }

    }

    @JsonbPropertyOrder({"riverHalfRetentionTime", "riverMaxRetentionFactor"})
    public static class Configuration extends Parameterizable {

        protected double riverHalfRetentionTime = 3;
        protected double riverMaxRetentionFactor = 0.3;

        @JsonbCreator
        public Configuration(@NotNull Parameter parameter) {
            super(parameter);
        }

        @PositiveOrZero
        public double getRiverHalfRetentionTime() {
            return this.riverHalfRetentionTime;
        }

        public void setRiverHalfRetentionTime(double time) {
            this.riverHalfRetentionTime = time;
        }

        @PositiveOrZero
        public double getRiverMaxRetentionFactor() {
            return this.riverMaxRetentionFactor;
        }

        public void setRiverMaxRetentionFactor(double factor) {
            this.riverMaxRetentionFactor = factor;
        }

    }

}
